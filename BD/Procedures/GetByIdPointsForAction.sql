-- =============================================
-- Author:		<Emir Mamashov>
-- Create date: <06.04.2016 11:07>
-- Email:		<emir.mamashov@gmail.com>
-- Description:	<GetByIdPointsForAction>
-- =============================================
Create Procedure [GetByIdPointsForAction]
	@Id nvarchar(128)
As
Begin
	Select *
	From PointsForAction
	Where Id = @Id
End
Go