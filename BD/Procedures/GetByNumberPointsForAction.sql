-- =============================================
-- Author:		<Emir Mamashov>
-- Create date: <06.04.2016 11:07>
-- Email:		<emir.mamashov@gmail.com>
-- Description:	<GetByNumberPointsForAction>
-- =============================================
Create Procedure [GetByNumberPointsForAction]
	@Number nvarchar(10)
As
Begin
	Select *
	From PointsForAction
	Where NumberPoint = @Number
End
Go