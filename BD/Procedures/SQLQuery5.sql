	declare @NumberPoint nvarchar(10)
	declare @FirstName nvarchar(256)
	declare @LastName nvarchar(256)
	declare @PhoneNumber nvarchar(12)
	declare @DateSendSms datetime

	declare @SortingProperty nvarchar(256) -- ����, �� �������� ����� ���� ����������
	declare @SortingDirection bit --����������� ����������: 0 - asc, 1 - desc
	declare @StartPosition int -- ��������� �������
	declare @PageSize int -- ������ ��������

	set @NumberPoint='100001'
	set @StartPosition=0
	set @PageSize=20

	declare @direction varchar(4)

	if @SortingDirection is null or @SortingDirection = 0 set @direction = 'asc'
	else 
		set @direction = 'desc'

	if @SortingProperty not in ('NumberPoint','FirstName','LastName','PhoneNumber','DateSendSms')
		set @SortingProperty = '[NumberPoint]'
	else
		set @SortingProperty = '[' + @SortingProperty + '] ' + @direction + ', [NumberPoint]'

	declare @script varchar(max)

	declare @scriptEnd varchar(max)


	if @NumberPoint!=null and @NumberPoint!=''
		set @script = '
		Select *
		From PointsForAction
		Where NumberPoint ='''+@NumberPoint+''''

	if @FirstName!=null and @FirstName!=''
		set @script+=' and FirstName='+@FirstName

	if @LastName!=null and @LastName!=''
		set @script+=' and LastName='+@LastName
		
	if @PhoneNumber!=null and @PhoneNumber!=''
		set @script+=' and PhoneNumber='+@PhoneNumber
		
	if @DateSendSms!=null and @DateSendSms!=''
		set @script+=' and DateSendSms='+@DateSendSms

	set @scriptEnd=@script+'order by ' + @SortingProperty + '
	offset '+ convert(nvarchar, @startPosition) +' rows
	fetch next '+ convert(nvarchar, @pageSize) +' rows only'

	execute(@scriptEnd)

	Select *
		From PointsForAction
		Where NumberPoint =100001

USE [ActionPetroleum]
GO

DECLARE	@return_value Int

EXEC	@return_value = [dbo].[SearchPointsForActionWithPogination]
		@NumberPoint = 100001,
		@FirstName = NULL,
		@LastName = NULL,
		@PhoneNumber = NULL,
		@DateSendSms = NULL,
		@SortingProperty = NULL,
		@SortingDirection = NULL,
		@StartPosition = 1,
		@PageSize = 1000000

SELECT	@return_value as 'Return Value'

GO
