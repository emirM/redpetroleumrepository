-- =============================================
-- Author:		<Emir Mamashov>
-- Create date: <07.04.2016 17:54>
-- Email:		<emir.mamashov@gmail.com>
-- Description:	<SearchPointsForActionWithPogination>
-- =============================================
Alter Procedure [dbo].[SearchPointsForActionWithPogination]
	@NumberPoint nvarchar(10),
	@FirstName nvarchar(256),
	@LastName nvarchar(256),
	@PhoneNumber nvarchar(12),
	@DateSendSms datetime,

	@SortingProperty nvarchar(256), -- ����, �� �������� ����� ���� ����������
	@SortingDirection bit, --����������� ����������: 0 - asc, 1 - desc
	@StartPosition int, -- ��������� �������
	@PageSize int -- ������ ��������
As
	declare @direction varchar(4)

	if @SortingDirection is null or @SortingDirection = 0 set @direction = 'asc'
	else 
		set @direction = 'desc'

	if @SortingProperty not in ('NumberPoint','FirstName','LastName','PhoneNumber','DateSendSms')
		set @SortingProperty = '[NumberPoint]'
	else
		set @SortingProperty = '[' + @SortingProperty + '] ' + @direction + ', [NumberPoint]'

	declare @script varchar(max)

	declare @scriptEnd varchar(max)

Begin
	if @NumberPoint!=null and @NumberPoint!=''
		declare @script varchar(max)
		set @script = '
		Select *
		From PointsForAction
		'

	--if @FirstName!=null and @FirstName!=''
	--	set @script+=' and FirstName='+@FirstName

	--if @LastName!=null and @LastName!=''
	--	set @script+=' and LastName='+@LastName
		
	--if @PhoneNumber!=null and @PhoneNumber!=''
	--	set @script+=' and PhoneNumber='+@PhoneNumber
		
	--if @DateSendSms!=null and @DateSendSms!=''
	--	set @script+=' and DateSendSms='+@DateSendSms

	--set @scriptEnd=
	--'Select *
	--	From PointsForAction
	--	Where NumberPoint='+@NumberPoint+'

	--order by ' + @SortingProperty + '
	--offset '+ convert(nvarchar, @startPosition) +' rows
	--fetch next '+ convert(nvarchar, @pageSize) +' rows only'

	exec('Select *
		From PointsForAction')
	--Select *
	--From PointsForAction
	--Where NumberPoint=@NumberPoint
End