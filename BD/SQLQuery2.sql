-- =============================================
-- Author:		<Emir Mamashov>
-- Create date: <14.04.2016 11:34>
-- Description:	<GetCountPointsForAction>
-- =============================================
--get count table 
CREATE PROCEDURE GetCountPointsForAction
	@count int OUTPUT
AS
	select @count = Count([FirstName])
	from PointsForAction
	where [FirstName]!=''
Return @count