/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 100000 [Id]
      ,[NumberPoint]
      ,[FirstName]
      ,[LastName]
      ,[PhoneNumber]
      ,[DateModified]
      ,[DateSendSms]
      ,[TransactionId]
      ,[TextXML]
  FROM [ActionPetroleum].[dbo].[PointsForAction]

Delete ActionPetroleum.dbo.PointsForAction
FROM ActionPetroleum.dbo.PointsForAction