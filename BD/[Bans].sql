Create table [Bans](
[Id] int Identity primary key,
[DateCreate] datetime not null,
[BanDate] datetime,
[PhoneNumber] nvarchar(16) not null,
[CountErrorSendSms] int not null
)