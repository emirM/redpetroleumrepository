USE [ActionPetroleum]
GO
/****** Object:  StoredProcedure [dbo].[GetPagiationsPointsActions]    Script Date: 05.05.2016 13:29:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Emir Mamashov>
-- Create date: <11.04.2016 10:40>
-- Description:	<GetPagiationsPointsActions>
-- =============================================
ALTER PROCEDURE [dbo].[GetPagiationsPointsActions]
	@startPosition int,
	@pageSize int,
	@Properties NVARCHAR(MAX),
	@Prioritets nvarchar(11)
AS
Begin
 --DECLARE @Properties NVARCHAR(MAX), @Prioritets nvarchar(11);
 declare @scriptStart NVARCHAR(MAX), @scriptEnd NVARCHAR(MAX), @where NVARCHAR(MAX), @resultQuery NVARCHAR(MAX);
 declare @NumberPoint nvarchar(10), @FirstName nvarchar(50), @LastName nvarchar(50), @PhoneNumber nvarchar(16), @DateSendSms nvarchar(20);
 declare @NumberPoint_Prioritet int, @FirstName_Prioritet int, @LastName_Prioritet int, @PhoneNumber_Prioritet int, @DateSendSms_Prioritet int;
 
 set @where='Where [FirstName]!='''''; 
 --set @Properties=',,,,,';

 --set @Prioritets='0,1,2,3,4';
--NamePoint
select @NumberPoint = Item
from dbo.SplitString(@Properties, ',')
Where ItemIndex=0
--NumberPoint_Prioritet
select @NumberPoint_Prioritet = Item 
from dbo.SplitString(@Prioritets, ',')
Where ItemIndex=0

--FirstName
select @FirstName = Item 
from dbo.SplitString(@Properties, ',')
Where ItemIndex=1
--@@FirstName_Prioritet
select @FirstName_Prioritet = Item 
from dbo.SplitString(@Prioritets, ',')
Where ItemIndex=1

--LastName
select @LastName = Item 
from dbo.SplitString(@Properties, ',')
Where ItemIndex=2
--@LastName_Prioritet
select @LastName_Prioritet = Item 
from dbo.SplitString(@Prioritets, ',')
Where ItemIndex=2

--PhoneNumber
select @PhoneNumber = Item 
from dbo.SplitString(@Properties, ',')
Where ItemIndex=3
--@PhoneNumber_Prioritet
select @PhoneNumber_Prioritet = Item 
from dbo.SplitString(@Prioritets, ',')
Where ItemIndex=3

--DateSendSms
select @DateSendSms = Item 
from dbo.SplitString(@Properties, ',')
Where ItemIndex=4
--@DateSendSms_Prioritet
select @DateSendSms_Prioritet = Item 
from dbo.SplitString(@Prioritets, ',')
Where ItemIndex=4

--po kakoi nado pervim delat Where
--1
If @NumberPoint_Prioritet=0
	Begin
		If @NumberPoint!=''
			set @where += ' and NumberPoint='+@NumberPoint;
	End
Else 
	Begin
		If @FirstName_Prioritet=0
			Begin
				If @FirstName!=''
					set @where+=' and FirstName='+@FirstName;
			End
		Else
			Begin
				If @LastName_Prioritet=0
					Begin
						If @LastName!=''
							set @where+=' and LastName='+@LastName;
					End
				Else
					Begin
						If @PhoneNumber_Prioritet=0
							Begin
								If @PhoneNumber!=''
									set @where+=' and PhoneNumber='+@PhoneNumber;
							End
						Else
							Begin
								if @DateSendSms_Prioritet=0
									Begin
										If @DateSendSms!=null and @DateSendSms!=''
											set @where+=' and DateSendSms='+@DateSendSms;
									End
							End
					End
			End
	End

--2
If @NumberPoint_Prioritet=1
	Begin
		If @NumberPoint!=''
			set @where += ' and NumberPoint='+@NumberPoint;
	End
Else 
	Begin
		If @FirstName_Prioritet=1
			Begin
				If @FirstName!=''
					set @where+=' and FirstName='+@FirstName;
			End
		Else
			Begin
				If @LastName_Prioritet=1
					Begin
						If @LastName!=''
							set @where+=' and LastName='+@LastName;
					End
				Else
					Begin
						If @PhoneNumber_Prioritet=1
							Begin
								If @PhoneNumber!=''
									set @where+=' and PhoneNumber='+@PhoneNumber;
							End
						Else
							Begin
								if @DateSendSms_Prioritet=1
									Begin
										If @DateSendSms!=''
											set @where+=' and DateSendSms='+@DateSendSms;
									End
							End
					End
			End
	End

--3
If @NumberPoint_Prioritet=2
	Begin
		If @NumberPoint!=''
			set @where += ' and NumberPoint='+@NumberPoint;
	End
Else 
	Begin
		If @FirstName_Prioritet=2
			Begin
				If @FirstName!=''
					set @where+=' and FirstName='+@FirstName;
			End
		Else
			Begin
				If @LastName_Prioritet=2
					Begin
						If @LastName!=''
							set @where+=' and LastName='+@LastName;
					End
				Else
					Begin
						If @PhoneNumber_Prioritet=2
							Begin
								If @PhoneNumber!=''
									set @where+=' and PhoneNumber='+@PhoneNumber;
							End
						Else
							Begin
								if @DateSendSms_Prioritet=2
									Begin
										If @DateSendSms!=''
											set @where+=' and DateSendSms='+@DateSendSms;
									End
							End
					End
			End
	End

--4
If @NumberPoint_Prioritet=3
	Begin
		If @NumberPoint!=''
			set @where += ' and NumberPoint='+@NumberPoint;
	End
Else 
	Begin
		If @FirstName_Prioritet=3
			Begin
				If @FirstName!=''
					set @where+=' and FirstName='+@FirstName;
			End
		Else
			Begin
				If @LastName_Prioritet=3
					Begin
						If @LastName!=''
							set @where+=' and LastName='+@LastName;
					End
				Else
					Begin
						If @PhoneNumber_Prioritet=3
							Begin
								If @PhoneNumber!=''
									set @where+=' and PhoneNumber='+@PhoneNumber;
							End
						Else
							Begin
								if @DateSendSms_Prioritet=3
									Begin
										If @DateSendSms!=''
											set @where+=' and DateSendSms='+@DateSendSms;
									End
							End
					End
			End
	End

--5
If @NumberPoint_Prioritet=4
	Begin
		If @NumberPoint!=''
			set @where += ' and NumberPoint='+@NumberPoint;
	End
Else 
	Begin
		If @FirstName_Prioritet=4
			Begin
				If @FirstName!=''
					set @where+=' and FirstName='+@FirstName;
			End
		Else
			Begin
				If @LastName_Prioritet=4
					Begin
						If @LastName!=''
							set @where+=' and LastName='+@LastName;
					End
				Else
					Begin
						If @PhoneNumber_Prioritet=4
							Begin
								If @PhoneNumber!=''
									set @where+=' and PhoneNumber='+@PhoneNumber;
							End
						Else
							Begin
								if @DateSendSms_Prioritet=4
									Begin
										If @DateSendSms!=''
											set @where+=' and DateSendSms='+@DateSendSms;
									End
							End
					End
			End
	End
--if @LastName!=null and @LastName!=''
--	set @where+=' and LastName='+@LastName
		
--if @PhoneNumber!=null and @PhoneNumber!=''
--	set @where+=' and PhoneNumber='+@PhoneNumber
		
--if @DateSendSms!=null and @DateSendSms!=''
--	set @where+=' and DateSendSms='+@DateSendSms

 --set @scriptStart = 'SELECT * FROM [PointsForAction] ';
 --set @scriptEnd=' ORDER BY Id';
 --set @where='Where [NumberPoint]!=0';
 set @resultQuery='SELECT * FROM [PointsForAction] '
 +@where
 +' ORDER BY [DateSendSms] Desc';
-- +' ORDER BY Id';
 
 DECLARE @handle int, @rows int--, @startPosition int, @pageSize int;
     
	 --print @NumberPoint_Prioritet
	 --print @FirstName_Prioritet
	 --print @where

 --set @startPosition=1;
 --set @pageSize=10;
  EXEC sp_cursoropen
    @handle OUT,
	@resultQuery,--'SELECT * FROM [PointsForAction] '+@where+' ORDER BY Id',
    1, -- 0x0001 - Keyset-driven cursor
    1, -- Read-only
    @rows OUT;-- SELECT @rows; -- Contains total rows count
 
  EXEC sp_cursorfetch
    @handle,
    16,        -- Absolute row index
    @startPosition,   -- Fetch from row
    @pageSize -- Rows count to fetch
 
EXEC sp_cursorclose @handle;
End
