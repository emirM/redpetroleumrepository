﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {

        public static bool isValidMail(string e_mail)
        {
            string expr =
              "[.\\-_a-z0-9]+@([a-z0-9][\\-a-z0-9]+\\.)+[a-z]{2,6}";

            Match isMatch =
              Regex.Match(e_mail, expr, RegexOptions.IgnoreCase);

            return isMatch.Success;
        }
        static void Main(string[] args)
        {
            // The input string again.
            string s = "Ред-xxxxxx     фамилия     имя?|56%^    ";
            string pattern = @"\s+";
            string target = " ";
            Regex regex = new Regex(pattern);
            string result = regex.Replace(s, target);

            var pattern3 = @"^\([a-z|A-Z|а-я|А-Я]{3})?[ ]?([0-9]{6})[ ]?([a-z|A-Z|а-я|А-Я])[ ]?([a-z|A-Z|а-я|А-Я])$";
            Regex regex2 = new Regex(pattern3);
            var result2 = regex2.IsMatch(s);
            Console.ReadKey();
        }
    }
}
