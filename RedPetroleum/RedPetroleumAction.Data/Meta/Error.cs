﻿// Author: Emir Mamashov
// Project: RedPetroleumAction.Data 
// DateCreate: 30.03.2016  17:14
// Location: Kyrgyzstan. Bishkek - 2016
namespace RedPetroleumAction.Data
{
    public class Error
    {
        public int Code { get; set; }
        public string Description { get; set; }
        public string Result { get; set; }
    }
}