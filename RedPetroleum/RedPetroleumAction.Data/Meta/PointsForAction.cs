﻿// Author: Emir Mamashov
// Project: RedPetroleumAction.Data 
// DateCreate: 29.03.2016  10:19
// Location: Kyrgyzstan. Bishkek - 2016

using System;
using System.ComponentModel.DataAnnotations;

namespace RedPetroleumAction.Data
{
    [MetadataType(typeof(PointsForActionMetaData))]
    public partial class PointsForAction
    {

    }

    public class PointsForActionMetaData
    {
        public string NumberPoint { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? DateModified { get; set; }
        public DateTime? DateSendSms { get; set; }
        public string DateSendSmsString { get; set; }
        public string TransactionId { get; set; }
        public string TextXML { get; set; }
        public bool IsConfirm { get; set; }
    }
}