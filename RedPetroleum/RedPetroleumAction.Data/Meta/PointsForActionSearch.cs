﻿// Author: Emir Mamashov
// Project: RedPetroleumAction.Data 
// DateCreate: 08.04.2016  10:04
// Location: Kyrgyzstan. Bishkek - 2016
namespace RedPetroleumAction.Data
{
    public class PointsForActionSearch
    {
        public PointsForAction PointsForAction { get; set; }
        public int StartPosition { get; set; }
        public int PageSize { get; set; }
    }
}