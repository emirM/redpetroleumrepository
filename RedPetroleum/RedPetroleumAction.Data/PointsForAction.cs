//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RedPetroleumAction.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class PointsForAction
    {
        public int Id { get; set; }
        public string NumberPoint { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public Nullable<System.DateTime> DateModified { get; set; }
        public Nullable<System.DateTime> DateSendSms { get; set; }
        public string TransactionId { get; set; }
        public string TextXML { get; set; }
        public Nullable<int> PhoneNumberInt { get; set; }
        public Nullable<long> PhoneNumberBigInt { get; set; }
        public Nullable<bool> IsConfirm { get; set; }

        public string DateSendSmsString { get; set; }
    }
}
