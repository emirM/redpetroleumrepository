﻿// Author: Emir Mamashov
// Project: RedPetroleumAction.Data 
// DateCreate: 29.03.2016  10:26
// Location: Kyrgyzstan. Bishkek - 2016

using System.Configuration;
using System.Data.Entity;

namespace RedPetroleumAction.Data.Repositories.GenericRepository
{
    public class Context<T> : IContext<T> where T : class
    {
        public DbContext DbContext { get; private set; }
        public IDbSet<T> DbSet { get; private set; }

        public Context()
        {
            DbContext = new DbContext(ConfigurationManager.ConnectionStrings["ActionPetroleumEntities"].ConnectionString);
            DbSet = DbContext.Set<T>();
        }

        public void Dispose()
        {
            DbContext.Dispose();
        }

    }
}