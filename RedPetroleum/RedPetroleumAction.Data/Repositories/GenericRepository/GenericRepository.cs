﻿// Author: Emir Mamashov
// Project: RedPetroleumAction.Data 
// DateCreate: 29.03.2016  10:29
// Location: Kyrgyzstan. Bishkek - 2016

using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;

namespace RedPetroleumAction.Data.Repositories.GenericRepository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly IContext<T> context;

        public GenericRepository()
        {
            context = new Context<T>();
        }
        public IQueryable<T> GetAll()
        {
            return context.DbSet;
        }

        public IQueryable<T> GetBy(Expression<Func<T, bool>> predicate)
        {
            var result = context.DbSet.Where(predicate);
            return result;
        }

        public T Add(T entity)
        {
            return context.DbSet.Add(entity);
        }

        public void Remove(T entity)
        {
            context.DbSet.Remove(entity);
        }

        public void Edit(T entity)
        {
            context.DbSet.Attach(entity);
            context.DbContext.Entry(entity).State = EntityState.Modified;
        }

        public void Save()
        {
            try
            {
                context.DbContext.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        public void Dispose()
        {
            context.DbContext.Dispose();
        }
    }
}