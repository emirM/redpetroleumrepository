﻿// Author: Emir Mamashov
// Project: RedPetroleumAction.Data 
// DateCreate: 29.03.2016  10:24
// Location: Kyrgyzstan. Bishkek - 2016

using System;
using System.Data.Entity;

namespace RedPetroleumAction.Data.Repositories.GenericRepository
{
    public interface IContext<T> : IDisposable where T : class
    {
        DbContext DbContext { get; }
        IDbSet<T> DbSet { get; }
    }
}