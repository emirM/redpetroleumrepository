﻿// Author: Emir Mamashov
// Project: RedPetroleumAction.Data 
// DateCreate: 29.03.2016  10:31
// Location: Kyrgyzstan. Bishkek - 2016

using System.Collections.Generic;
using System.Threading.Tasks;
using RedPetroleumAction.Data.Repositories.GenericRepository;

namespace RedPetroleumAction.Data.Repositories
{
    #region IPointsForActionRepository
    public interface IPointsForActionRepository : IGenericRepository<PointsForAction>
    {
        Task<PointsForAction> GetByNumber(string pointsForActionNumber);
        List<PointsForAction> GetAllProc();
        List<PointsForAction> GetPagiationsPointsActions(int? startPosition, int? pageSize, string properties, string prioritets);
        int GetCountPointsAction();
    }
    #endregion
    #region IBansRepository

    public interface IBansRepository : IGenericRepository<Ban>
    {
        
    }
    #endregion
}