﻿// Author: Emir Mamashov
// Project: RedPetroleumAction.Data 
// DateCreate: 29.03.2016  10:32
// Location: Kyrgyzstan. Bishkek - 2016

using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using RedPetroleumAction.Data.Repositories.GenericRepository;

namespace RedPetroleumAction.Data.Repositories
{
    #region PointsForActionRepository
    public class PointsForActionRepository : GenericRepository<PointsForAction>, IPointsForActionRepository
    {
        private ActionPetroleumEntities _context = new ActionPetroleumEntities();
        public async Task<PointsForAction> GetByNumber(string pointsForActionNumber)
        {
            var para = new SqlParameter("@Number", pointsForActionNumber);
            return await _context.Database.SqlQuery<PointsForAction>("GetByNumberPointsForAction @Number", para).FirstOrDefaultAsync();
        }

        public List<PointsForAction> GetPagiationsPointsActions(int? startPosition, int? pageSize, string properties, string prioritets)
        {
            var para = new SqlParameter("@startPosition", startPosition);
            var para2 = new SqlParameter("@pageSize", pageSize);
            var para3 = new SqlParameter("@Properties", properties);
            var para4 = new SqlParameter("@Prioritets", prioritets);
            var result = _context.Database.SqlQuery<PointsForAction>("GetPagiationsPointsActions @startPosition, @pageSize, @Properties, @Prioritets", para, para2, para3, para4).ToList();
            return result;
        }

        public List<PointsForAction> GetAllProc()
        {
            return _context.Database.SqlQuery<PointsForAction>("GetAllPointsForAction").ToList(); ;
        }

        public int GetCountPointsAction()
        {
            ObjectParameter count = new ObjectParameter("count", typeof(int));
            count.Value = 0;
            _context.GetCountPointsForAction(count);
            return (int)count.Value;
        }
    }
    #endregion
    #region BansRepository

    public class BansRepository : GenericRepository<Ban>, IBansRepository
    {
        
    }
    #endregion
}