﻿using System.Data.Entity;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using RedPetroleumAction.Data.Repositories;

namespace RedPetroleumActionWebApi.Controllers
{
    public class BansController : Controller
    {
        private IBansRepository _bansRepository;

        public BansController(IBansRepository bansRepository)
        {
            _bansRepository = bansRepository;
        }

        // GET: Bans
        public async Task<ActionResult> Index()
        {
            return View(await _bansRepository.GetAll().ToListAsync());
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _bansRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
