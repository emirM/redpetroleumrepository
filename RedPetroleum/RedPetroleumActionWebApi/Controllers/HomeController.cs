﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using RedPetroleumAction.Data;
using RedPetroleumAction.Data.Repositories;

namespace RedPetroleumActionWebApi.Controllers
{
    public class HomeController : Controller
    {
        private IPointsForActionRepository _pointsForActionRepository;

        public HomeController(IPointsForActionRepository pointsForActionRepository)
        {
            _pointsForActionRepository = pointsForActionRepository;
        }
        public ActionResult Index()
        {
            //return Redirect("/web");
            //return RedirectToAction("Index", "Help");
            ViewBag.Title = "Home Page";
            //ViewBag.ResultValue = result;
            //ViewBag.PointsForAction = _pointsForActionRepository.GetBy(x => !String.IsNullOrEmpty(x.FirstName)).ToList();
            
            return View();
        }

        private void InsertPointNumber(int stratIndex, int endIndex)
        {
            for (var i = stratIndex; i <= endIndex; i++)
            {
                var stringInt = i + 1;
                var pointForAction = new PointsForAction()
                {
                    NumberPoint = stringInt.ToString()
                };
                _pointsForActionRepository.Add(pointForAction);
            }

            _pointsForActionRepository.Save();
        } 

        private bool IsNum(string s)
        {
            foreach (char c in s)
            {
                if (!Char.IsDigit(c)) return false;
            }
            return true;
        }

    }
}
