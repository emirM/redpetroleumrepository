﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Xml;
using System.Xml.Linq;
using RedPetroleumAction.Data;
using RedPetroleumAction.Data.Repositories;

namespace RedPetroleumActionWebApi.Controllers
{

    public class ValuesController : ApiController
    {
        private IPointsForActionRepository _pointsForActionRepository;
        private IBansRepository _bansRepository;

        public ValuesController(IPointsForActionRepository pointsForActionRepository, IBansRepository bansRepository)
        {
            _pointsForActionRepository = pointsForActionRepository;
            _bansRepository = bansRepository;
        }
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
                return "value";
        }

        // POST api/values
        //public async Task<IHttpActionResult> Post([FromBody]string value)
        //{
        //    var result = value;
        //    return Ok(result);
        //}
        
        public IHttpActionResult Post(HttpRequestMessage request)
        {
            var doc = new XmlDocument();
            doc.Load(request.Content.ReadAsStreamAsync().Result);
            //var pointForAction = new PointsForAction()
            //{
            //    DateModified = DateTime.Now,
            //    DateSendSms = DateTime.Now,
            //    NumberPoint = "923823",
            //    FirstName = "",
            //    LastName = "",
            //    PhoneNumber = "",
            //    TransactionId = "123",
            //    TextXML = doc.InnerXml
            //};
            //_pointsForActionRepository.Add(pointForAction);
            //_pointsForActionRepository.Save();

            var doc2 = XDocument.Parse(doc.InnerXml);
            foreach (XElement messeges in doc2.Elements())
            {
                //выводим в цикле названия всех дочерних элементов и их значения
                foreach (XElement message in messeges.Elements())
                {
                    var messegesElements = message.Elements().ToArray();
                    var phoneNumber = messegesElements[5].Value;
                    var dbBan =_bansRepository.GetBy(x => x.PhoneNumber == phoneNumber).FirstOrDefault();
                    if (dbBan == null || dbBan.BanDate == null || dbBan.BanDate < DateTime.Now)//esli ego v spiske banov ili date bana<date.now
                    {
                        var banCount = dbBan == null ? 1 : 2;
                        var resultOperation = AddPoint(message.Elements().ToArray(), banCount);//togda otpravlyaem
                        if (resultOperation.Code != 0) //esli oshibochniy cod vozvrashen
                        {
                            //nahodim v spiske li banov danniy nomer
                            if (dbBan != null) //esli est
                            {
                                if (dbBan.CountErrorSendSms >= 2) //i ego oshibochnie otpravki ravno 2 ili bolshe 
                                {
                                    dbBan.BanDate = DateTime.Now.AddDays(1); //tegda ego banim
                                    dbBan.TextMessegeHtml += "<p><h3> Попытка #2</h3>" +
                                                             "<h4>Текст СМС отправленный клиентом:</h4>" +
                                                            HttpUtility.UrlDecode(messegesElements[6].Value) + "<br/>" +
                                                             "<h4>Ответный СМС клиенту:</h4>" +
                                                             resultOperation.Description + "<br/>" +
                                                             "</p>";
                                }
                                else //inache
                                {
                                    dbBan.CountErrorSendSms += 1; //dobavlyaem ban
                                    if (dbBan.CountErrorSendSms >= 2) //esli stalo colichestvo oshibochnih otpravok 2 togda
                                    {
                                        dbBan.BanDate = DateTime.Now.AddDays(1); //banim ego
                                        dbBan.TextMessegeHtml += "<p><h3> Попытка #2</h3>" +
                                                             "<h4>Текст СМС отправленный клиентом:</h4>" +
                                                             HttpUtility.UrlDecode(messegesElements[6].Value) + "<br/>" +
                                                             "<h4>Ответный СМС клиенту:</h4>" +
                                                             resultOperation.Description + "<br/>" +
                                                             "</p>";
                                    }
                                }
                                _bansRepository.Edit(dbBan);
                            }
                            else //esli net v spiske bana
                            {
                                var ban = new Ban()//dobavlyaem ego v spisok banov
                                {
                                    DateCreate = DateTime.Now,
                                    //BanDate = DateTime.Now.AddDays(24),
                                    PhoneNumber = messegesElements[5].Value,
                                    CountErrorSendSms = 1,
                                    TextMessegeHtml = "<p><h3>Попытка #1</h3>" +
                                                  "<h4>Текст СМС отправленный клиентом:</h4>"+ HttpUtility.UrlDecode(messegesElements[6].Value) + "<br/>" +
                                                  "<h4>Ответный СМС клиенту:</h4>" + resultOperation.Description+"<br/>"+
                                                  "</p>"
                                };
                                _bansRepository.Add(ban);
                            }

                        }
                        else//esli uspeshniy cod vozvrashen 
                        {
                            if (dbBan != null)//esli est v spiske banov
                            {
                                _bansRepository.Remove(dbBan);
                            }
                        }
                    }
                }
            }
            _bansRepository.Save();
            //var result =AddPoints(doc.InnerXml);
            return Ok();
        }

        private Error AddPoint(XElement[] messegesElements,int banCount)
        {
            var result = new Error() { Code = 0, Description = "Вы успешно зареганы!" };

            var transactionId = messegesElements[0].Value;
            var time = messegesElements[1].Value;//time=time[0]
            var phone = messegesElements[5].Value;
            var text = messegesElements[6].Value;//full text=text[0]

            
            var notCorrectFormatErrorMsg = "";
            var notFoundThisNumberPoint = "";
            if (banCount == 1)
            {
                notCorrectFormatErrorMsg = "Neverniy format. Proverte pravilnost dannih i otpravte v formate RED-XXXXXX(6 cifr), (ostalas’ 1 popitka). Spasibo za uchastie. Red Petroleum.";
                notFoundThisNumberPoint = "(ostalas’ 1 popitka)";
            }
            else if (banCount == 2)
            {
                notCorrectFormatErrorMsg = "Neverniy format. Vash nomer telefona zablokirovan na 24 chasa. Red Petroleum.";
                notFoundThisNumberPoint = "Vash nomer telefona zablokirovan na 24 chasa.";
            }

            var decodeText = HttpUtility.UrlDecode(text);
            string pattern = @"\s+";
            string target = " ";
            Regex regex = new Regex(pattern);
            string textRes = regex.Replace(decodeText, target);
            var pointNumber = "";
            var firstName = "";
            var lastName = "";
            if (textRes.Count(x => x == ' ') < 2)
            {
                result.Code = 2;
                result.Description = notCorrectFormatErrorMsg;
                SendSms(transactionId, notCorrectFormatErrorMsg);
                return result;
            }
            if (textRes.Count(x => x == '-') > 0)
            {
                if (text.Count(x => x == '-') > 1)
                {
                    result.Code = 1;
                    result.Description = notCorrectFormatErrorMsg;
                    SendSms(transactionId, notCorrectFormatErrorMsg);
                    return result;
                }
                var textSplit = textRes.Split('-');
                var textSplit2 = textSplit[1].Split(' ');
                if (textSplit2.Length < 3)
                {
                    result.Code = 3;
                    result.Description = notCorrectFormatErrorMsg;
                    SendSms(transactionId, notCorrectFormatErrorMsg);//"Nomer RED-" + HttpUtility.UrlDecode(textSplit2[0]) + " neverniy. Proverte pravilnost dannih i otpravte v formate RED-XXXXXX(6 cifr). "+ notFoundThisNumberPoint + " Spasibo za uchastie.Red Petroleum.");
                    return result;
                }
                pointNumber = textSplit2[0];
                firstName = textSplit2[2];
                lastName = textSplit2[1];
            }
            else
            {
                var textSplit = textRes.Split(' ');

                pointNumber = textSplit[1];
                if (textSplit.Length < 4)
                {
                    result.Code = 3;
                    result.Description = notCorrectFormatErrorMsg;
                    SendSms(transactionId, notCorrectFormatErrorMsg);//"Nomer RED-" + HttpUtility.UrlDecode(textSplit2[0]) + " neverniy. Proverte pravilnost dannih i otpravte v formate RED-XXXXXX(6 cifr). "+ notFoundThisNumberPoint + " Spasibo za uchastie.Red Petroleum.");
                    return result;
                }
                firstName = textSplit[3];
                lastName = textSplit[2];
            }
            
            var numberPoint = _pointsForActionRepository.GetBy(x => x.NumberPoint == pointNumber).FirstOrDefault();
            if (numberPoint == null)
            {
                result.Code = 3;
                result.Description = notCorrectFormatErrorMsg;
                SendSms(transactionId, notCorrectFormatErrorMsg);//"Nomer RED-" + HttpUtility.UrlDecode(textSplit2[0]) + " neverniy. Proverte pravilnost dannih i otpravte v formate RED-XXXXXX(6 cifr). "+ notFoundThisNumberPoint + " Spasibo za uchastie.Red Petroleum.");
                return result;
            }
            else if (numberPoint.FirstName != null || numberPoint.LastName != null || numberPoint.PhoneNumber != null)
            {
                result.Code = 4;
                result.Description = "Kupon RED-" + pointNumber + " uje bil zaregestrirovan. Vvedite pravilniy nomer." + notFoundThisNumberPoint + " Red Petroleum.";
                SendSms(transactionId, "Kupon RED-" + pointNumber + " uje bil zaregestrirovan. Vvedite pravilniy nomer." + notFoundThisNumberPoint + " Red Petroleum.");
                return result;
            }
            SendSms(transactionId, "Kupon RED-" + pointNumber + " zaregestrirovan. Spasibo za uchastie v akcii \"Fitomania\". Rozigrish 30/08/2016 v 19:00 na kanale NTS. Udachi Vam! Red Petroleum.");

            numberPoint.DateModified = DateTime.Now;
            numberPoint.DateSendSms = Convert.ToDateTime(time);
            //numberPoint.NumberPoint = textSplit2[0];
            numberPoint.FirstName = firstName;
            numberPoint.LastName = lastName;
            numberPoint.PhoneNumber = phone;
            numberPoint.TransactionId = transactionId;

            _pointsForActionRepository.Edit(numberPoint);
            _pointsForActionRepository.Save();

            return result;
        }
        /// <summary>
        /// Check number point in DB
        /// </summary>
        /// <param name="pointsString"></param>
        /// <returns></returns>
        private Error AddPoints(string pointsString)
        {
            var result = new Error() {Code = 0, Description = "Вы успешно зареганы!"};
            var splitOnePoints = pointsString.Split('>');

            var transactionId = splitOnePoints[4].Split('<');
            var time = splitOnePoints[6].Split('<');//time=time[0]
            var phone = splitOnePoints[14].Split('<');//phone=phone[0]
            var text = splitOnePoints[16].Split('<');//full text=text[0]
            if (text[0].Count(x => x == '-') != 1)
            {
                result.Code = 1;
                result.Description = "Неправильный формат сообщения!";
                result = SendSms(transactionId[0], "Neverniy format. Proverte pravilnost dannih i otpravte v formate RED-XXXXXX(6 cifr). Spasibo za uchastie. Red Petroleum.");
                return result;
            }

            var decodeText = HttpUtility.UrlDecode(text[0]);
            var textSplit = decodeText.Split('-');

            if (textSplit[1].Count(x => x == ' ') < 2)
            {
                result.Code = 2;
                result.Description = "Неправильный формат сообщения!";
                result = SendSms(transactionId[0], "Neverniy format. Proverte pravilnost dannih i otpravte v formate RED-XXXXXX(6 cifr). Spasibo za uchastie. Red Petroleum.");
                return result;
            }

            var textSplit2 = textSplit[1].Split(' ');
            var pointNumber = textSplit2[0];
            var numberPoint = _pointsForActionRepository.GetBy(x => x.NumberPoint == pointNumber).FirstOrDefault();
            if (numberPoint == null)
            {
                result.Code = 3;
                result.Description = "Такого купона не существует!";
                result = SendSms(transactionId[0], "Nomer RED-" + HttpUtility.UrlDecode(textSplit2[0]) + " neverniy. Proverte pravilnost dannih i otpravte v formate RED-XXXXXX(6 cifr). Spasibo za uchastie.Red Petroleum.");
                return result;
            }
            else if(numberPoint.FirstName!=null || numberPoint.LastName!=null || numberPoint.PhoneNumber!=null)
            {
                result.Code = 4;
                result.Description = "Такой купон существует!";
                result = SendSms(transactionId[0], "Kupon RED-"+ textSplit2[0] + " uje bil zaregestrirovan. Vvedite pravilniy nomer. Red Petroleum.");
                return result;
            }
            result = SendSms(transactionId[0], "Kupon RED-" + textSplit2[0] + " zaregestrirovan. Spasibo za uchastie v akcii \"Fitomania\". Rozigrish 30/08/2016 v 19:00 na kanale NTS. Udachi Vam! Red Petroleum.");

            //var pointForAction = new PointsForAction()
            //{
            //    DateModified = DateTime.Now,
            //    DateSendSms = Convert.ToDateTime(time[0]),
            //    NumberPoint = textSplit2[0],
            //    FirstName = textSplit2[1],
            //    LastName = textSplit2[2],
            //    PhoneNumber = phone[0],
            //    TransactionId = transactionId[0]
            //};
            //_pointsForActionRepository.Add(pointForAction);

            numberPoint.DateModified = DateTime.Now;
            numberPoint.DateSendSms = Convert.ToDateTime(time[0]);
            //numberPoint.NumberPoint = textSplit2[0];
            numberPoint.FirstName = textSplit2[2];
            numberPoint.LastName = textSplit2[1];
            numberPoint.PhoneNumber = phone[0];
            numberPoint.TransactionId = transactionId[0];

            _pointsForActionRepository.Edit(numberPoint);
            _pointsForActionRepository.Save();

            return result;
        }

        private Error SendSms(string transactionId, string messegeText)
        {
            var result = new Error() {Code = 1, Description = "error!"};
            var xdoc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"));
            
            xdoc.Add(new XElement("response",
                            new XElement("authentication", 
                                new XElement("login", "crmtech"),
                                new XElement("password", "z7FLK")),//Login партнера, выдаваемый ему при создании аккаунта 
                            new XElement("message", 
                                new XElement("transaction-id", transactionId),
                                new XElement("text", messegeText)
                            )
                        ));
            /* отправку данного sms типа XML серверу*/

            var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"+xdoc;
            var url = "http://212.42.102.215:2912";
            var req = (HttpWebRequest)WebRequest.Create(url);

            var requestBytes = Encoding.UTF8.GetBytes(xml);
            req.Method = "POST";
            req.ContentType = "text/xml;";
            req.ContentLength = requestBytes.Length;
            var requestStream = req.GetRequestStream();
            requestStream.Write(requestBytes, 0, requestBytes.Length);
            requestStream.Close();

            WebResponse wresp = null;
            try
            {
                wresp = req.GetResponse();
                Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                var read = reader2.ReadToEnd();
                result.Code=0;
                result.Description = read;
                return result;
                //Console.WriteLine(string.Format("File uploaded, server response is: {0}", reader2.ReadToEnd()));
            }
            catch (Exception ex)
            {
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                    result.Code = 2;
                    result.Description = ex.Message;
                    return result;
                }
            }
            finally
            {
                req = null;
            }
            return result;
        }
        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
