﻿function searchOld(points, searchText, indexSearch) {
    ///<summary>poisk
    ///points-elementi po kotorim budet osushestevlena poisk
    ///searchText-iskomiy text
    ///indexSearch - index poiska, to est posledovatelnost poiska. kakoi perviy, vtoroi
    ///</summary>
    searchText.parentElement.children[1].value = indexSearch;//indexSearch
    searchText.parentElement.children[2].value = 1;//isSearch
    for (var i = 0; i < points.length; i++) {
        var res = points[i].children[searchText.parentElement.cellIndex].innerText;
        var searchtext = searchText.value;
        var result = res.search(new RegExp(searchtext, "i"));
        if (result >=0) {
            points[i].style.display = "";
        } else {
            points[i].style.display = "none";
        }
    }
}

function changedInputValueOld(idInput) {
    var inputVal = $("#" + idInput);

    if (inputVal.length > 0) {
        var allPoints = $(".points");//vitaskivaem vse poiti
        var points = JSLINQ(allPoints).Where(function (item) { return item.style.display != "none" });

        var indexSearchs = $(".isSearch");
        var isSearch = JSLINQ(indexSearchs).Where(function (item) { return item.value == 1 });//ishem bilo do etogo osushestevlen poisk
        var orderBy_indexSearchsIsSearch = JSLINQ(isSearch.items).OrderBy(function (item) { return item.parentElement.children[1].value });//sortirovka index poiska\

        if (points.items.length > 0) {//esli oni est
           
            if (isSearch.items.length > 0) { //esli da to
                //inputVal[0].parentElement.children[2].value - znachenie isSearch
                if (inputVal[0].value == "" || inputVal[0].parentElement.children[2].value==1) {//esli iskomoe znachenie pustoe ili po nim uje bil poisk togda poisk bydet zanogog
                    //if (inputVal[0].parentElement.children[2].value == 0) {
                        //poisk zanogo
                    for (var i = 0; i < orderBy_indexSearchsIsSearch.items.length; i++) {
                        if (i > 0) {
                            var pointsFilter = JSLINQ(allPoints).Where(function (item) { return item.style.display != "none" });
                            search(pointsFilter.items, orderBy_indexSearchsIsSearch.items[i].parentElement.children[0], i);
                        } else {
                            search(allPoints, orderBy_indexSearchsIsSearch.items[i].parentElement.children[0], i);
                        }
                   }
                    //}
                } else {
                    search(points.items, inputVal[0], orderBy_indexSearchsIsSearch.items.length);
                }
            } else {//esli net
                if (inputVal[0].value != "") {
                    search(points.items, inputVal[0], 0);
                }
            }
        } else if (allPoints.length > 0) {
            for (var j = 0; j < orderBy_indexSearchsIsSearch.items.length; j++) {
                if (j > 0) {
                    var pointsFilter2 = JSLINQ(allPoints).Where(function (item) { return item.style.display != "none" });
                    search(pointsFilter2.items, orderBy_indexSearchsIsSearch.items[j].parentElement.children[0], j);
                } else {
                    search(allPoints, orderBy_indexSearchsIsSearch.items[j].parentElement.children[0], j);
                }
            }
        }
    }
}

