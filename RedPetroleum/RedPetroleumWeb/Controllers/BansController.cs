﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using RedPetroleumAction.Data.Repositories;

namespace RedPetroleumWeb.Controllers
{
    [Authorize]
    public class BansController : Controller
    {
        private IBansRepository _bansRepository;

        public BansController(IBansRepository bansRepository)
        {
            _bansRepository = bansRepository;
        }

        // GET: Bans
        public async Task<ActionResult> Index()
        {
            return View(await _bansRepository.GetAll().OrderByDescending(x=>x.DateCreate).ToListAsync());
        }

        // GET: Projects/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var ban = await _bansRepository.GetBy(x => x.Id == id).FirstOrDefaultAsync();
            if (ban == null)
            {
                return HttpNotFound();
            }
            return View(ban);
        }

        // POST: Projects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            if (id == null)
            {
                return View();
            }
            var ban = await _bansRepository.GetBy(x => x.Id == id).FirstOrDefaultAsync();
            if (ban == null) return View();
            _bansRepository.Remove(ban);
            _bansRepository.Save();
            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _bansRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
