﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using RedPetroleumAction.Data;
using RedPetroleumAction.Data.Repositories;

namespace RedPetroleumWeb.Controllers
{
    
    public class HomeController : Controller
    {
        private IPointsForActionRepository _pointsForActionRepository;

        public HomeController(IPointsForActionRepository pointsForActionRepository)
        {
            _pointsForActionRepository = pointsForActionRepository;
        }

        [Authorize]
        public ActionResult Index()
        {
            
            ViewBag.Title = "Home Page";
            var pointsForActionSearch = GetPointsPogination(1, 20, ",,,,,", "0,1,2,3,4");
            //var pointsForActionSearch2 = _pointsForActionRepository.GetAllProc();
            var count = _pointsForActionRepository.GetCountPointsAction();
            ViewBag.PointsForAction = pointsForActionSearch;//_pointsForActionRepository.GetBy(x => !String.IsNullOrEmpty(x.FirstName)).OrderByDescending(x=>x.DateSendSms).ToList();
            double fullPage = (double)_pointsForActionRepository.GetCountPointsAction() / 20;

            ViewBag.FullCountPage = fullPage <= 0?1:Math.Ceiling(fullPage);
            //ViewBag.PointsForAction = await _pointsForActionRepository.GetAllProc();
            //var messegeXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><messages><message><transaction-id>32788982</transaction-id><time>31-03-2016 17:09:14.476</time><keyword activated=\"false\">RED</keyword><provider>nurtel</provider><short-number>7115</short-number><phone>996705888670</phone><text>Red-100001+%D0%9A%D0%B8%D0%BC+%D0%A7%D0%B8%D0%BD%D0%BC%D0%B8%D0%BD</text></message></messages>";

            //var splitOnePoints = messegeXml.Split('>');

            //var transactionId = splitOnePoints[4].Split('<');
            //var time = splitOnePoints[6].Split('<');
            //var phone = splitOnePoints[14].Split('<');
            //var text = splitOnePoints[16].Split('<');
            //var textSplit = text[0].Split('-');
            //var textSplit2 = textSplit[1].Split(' ');
            //var decodeText = HttpUtility.UrlDecode(text[0]);

            //InsertPointNumber(100000, 112485);
            //InsertPointNumber(212485, 25464);
            //InsertPointNumber(345655, 375102);
            //InsertPointNumber(475102, 488542);

            return View();
        }
        private List<PointsForAction> GetPointsPogination(int? startPosition, int? pageSize, string properties, string prioritets)
        {
            var pointsActions = new List<PointsForAction>();
            if (startPosition <= 0 || startPosition == null) return pointsActions;
            if (pageSize <= 0 || pageSize == null) return pointsActions;
            if (String.IsNullOrEmpty(properties)) return pointsActions;
            if (String.IsNullOrEmpty(prioritets)) return pointsActions;
            // Создание открытого подключения
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = @"Data Source=176.123.237.242;Initial Catalog=ActionPetroleum;" +
                    "User Id=AutelService;Password=Version2;";
                conn.Open();

                //SqlCommand command = new SqlCommand("SELECT * FROM [PointsForAction] ORDER BY Id", conn);
                //SqlDataReader reader2 = command.ExecuteReader();
                //// Пройти в цикле по записям и построить HTML-строку
                //var res2 = "";
                //while (reader2.Read())
                //{
                //    res2 += String.Format("<li>{0} <b>{1}</b> {2} - работает</li>",
                //        reader2["NumberPoint"], reader2.GetString(1), reader2.GetString(2));
                //}
                using (var comm = new SqlCommand("dbo.GetPagiationsPointsActions", conn))
                {
                    comm.CommandType = CommandType.StoredProcedure;
                    comm.Parameters.Add(new SqlParameter("@startPosition", startPosition));
                    comm.Parameters.Add(new SqlParameter("@pageSize", pageSize));
                    comm.Parameters.Add(new SqlParameter("@Properties", properties));
                    comm.Parameters.Add(new SqlParameter("@Prioritets", prioritets));
                    using (var reader = comm.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Response.Write(reader["id"]);
                            Response.Write(reader["level"]);
                        }

                        var ok = reader.NextResult(); // <---

                        if (!ok) return pointsActions;

                        if (reader.Read())
                        {
                            //var val = reader["NumberPoint"];var datetime = string.IsNullOrEmpty(reader["DateSendSms"].ToString()) ? null : (DateTime?)reader["DateSendSms"];
                            var datetimeOne = string.IsNullOrEmpty(reader["DateSendSms"].ToString()) ? null : (DateTime?)reader["DateSendSms"];
                            var isConfirm = string.IsNullOrEmpty(reader["IsConfirm"].ToString()) ? null : (bool?)reader["IsConfirm"];
                            //var id = string.IsNullOrEmpty(reader["Id"].ToString()) ? null : (int?)reader["Id"];
                            Response.Write(reader["NumberPoint"]);
                            Response.Write(reader["Id"]);
                            pointsActions.Add(new PointsForAction()
                            {
                                DateSendSms = datetimeOne,
                                DateModified = datetimeOne,
                                DateSendSmsString = datetimeOne.ToString(),
                                FirstName = string.IsNullOrEmpty(reader["FirstName"].ToString()) ? null : (string)reader["FirstName"],
                                LastName = string.IsNullOrEmpty(reader["LastName"].ToString()) ? null : (string)reader["LastName"],
                                NumberPoint = string.IsNullOrEmpty(reader["NumberPoint"].ToString()) ? null : (string)reader["NumberPoint"],
                                PhoneNumber = string.IsNullOrEmpty(reader["PhoneNumber"].ToString()) ? null : (string)reader["PhoneNumber"],
                                Id = (int)reader["Id"],
                                IsConfirm = isConfirm,
                                TransactionId = string.IsNullOrEmpty(reader["TransactionId"].ToString()) ? null : (string)reader["TransactionId"]

                            });
                            //var res = "";
                            while (reader.Read())
                            {
                            //    res += String.Format("<li>{0} <b>{1}</b> {2} - работает{3}</li>",
                            //        reader["NumberPoint"], reader.GetValue(1), reader.GetValue(2), reader.GetValue(3));
                                var datetime = string.IsNullOrEmpty(reader["DateSendSms"].ToString()) ? null : (DateTime?)reader["DateSendSms"];
                                var isConfirmItem = string.IsNullOrEmpty(reader["IsConfirm"].ToString()) ? null : (bool?)reader["IsConfirm"];

                                pointsActions.Add(new PointsForAction()
                                {
                                    DateSendSms = datetime,
                                    DateModified = datetime,
                                    DateSendSmsString = datetime.ToString(),
                                    FirstName = string.IsNullOrEmpty(reader["FirstName"].ToString()) ? null : (string)reader["FirstName"],
                                    LastName = string.IsNullOrEmpty(reader["LastName"].ToString()) ? null : (string)reader["LastName"],
                                    NumberPoint = string.IsNullOrEmpty(reader["NumberPoint"].ToString()) ? null : (string)reader["NumberPoint"],
                                    PhoneNumber = string.IsNullOrEmpty(reader["PhoneNumber"].ToString()) ? null : (string)reader["PhoneNumber"],
                                    Id = (int)reader["Id"],
                                    IsConfirm = isConfirmItem,
                                    TransactionId = string.IsNullOrEmpty(reader["TransactionId"].ToString()) ? null : (string)reader["TransactionId"]

                                });
                            }
                        }
                    }
                }
                // Работа с базой данных
                conn.Close();
            }
            return pointsActions;
        }

        private void InsertPointNumber(int stratIndex, int endIndex)
        {
            for (var i = stratIndex; i <= endIndex; i++)
            {
                var stringInt = i;
                var pointForAction = new PointsForAction()
                {
                    NumberPoint = stringInt.ToString()
                };
                _pointsForActionRepository.Add(pointForAction);
            }

            _pointsForActionRepository.Save();
        }

        private void DeletePointNumber(int stratIndex, int endIndex)
        {
            for (var i = stratIndex; i <= endIndex; i++)
            {
                var stringInt = i;
                var pointForAction = _pointsForActionRepository.GetBy(x => x.NumberPoint == i.ToString()).FirstOrDefault();
                _pointsForActionRepository.Remove(pointForAction);
            }
            
            _pointsForActionRepository.Save();
        }

        private bool IsNum(string s)
        {
            foreach (char c in s)
            {
                if (!Char.IsDigit(c)) return false;
            }
            return true;
        }

        public ActionResult About()
        {
            ViewBag.Message = "О компании.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact page.";
            //InsertPointNumber(100001, 112485);
            //InsertPointNumber(188543, 188548);
            //InsertPointNumber(188583, 188723);
            //InsertPointNumber(212486, 254655);
            //InsertPointNumber(288549, 288567);
            //InsertPointNumber(288724, 289199);
            //InsertPointNumber(354656, 375102);
            //InsertPointNumber(388568, 388576);
            //InsertPointNumber(389200, 389430);
            //InsertPointNumber(475103, 488542);
            //InsertPointNumber(488577, 488582);
            //InsertPointNumber(489431, 489582);
            return View();
        }

        public ActionResult CheckPointsIsRegister()
        {
            return View();
        }


        /// <summary>
        /// Proverka nomera kupona na registraziui
        /// 
        /// <code>return:
        /// 0-ok;
        /// 1-pointNumber is null;
        /// 2-not found;
        /// 3-this point not register;</code>
        /// </summary>
        /// <param name="pointNumber"></param>
        /// <returns></returns>
        public async Task<ActionResult> CheckPointNumber(string pointNumber)
        {
            var errorResult=new Error() {Code = 0, Description = "ok"};
            if (String.IsNullOrEmpty(pointNumber))
            {
                errorResult.Code = 1;
                errorResult.Description = "pointNumber is null";
                return Json(errorResult, JsonRequestBehavior.AllowGet);
            }
            var point = await _pointsForActionRepository.GetByNumber(pointNumber);
            if (point == null)
            {
                errorResult.Code = 2;
                errorResult.Description = "Такого номера купона не существует!";
                return Json(errorResult, JsonRequestBehavior.AllowGet);
            }
            if (point.FirstName==null)
            {
                errorResult.Code = 3;
                errorResult.Description = "Этот купон еще не зарегистрирован.";
                return Json(errorResult, JsonRequestBehavior.AllowGet);
            }
            point.DateSendSmsString = point.DateSendSms.ToString();
            var jsSerialz = new JavaScriptSerializer { MaxJsonLength = 100000000 };
            var jso = jsSerialz.Serialize(point);
            errorResult.Result = jso;
            return Json(errorResult, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// <code>
        /// return:
        /// 1-ok
        /// 2-pageSize is not valid
        /// 3-properties is null
        /// 4-prioritets is null
        /// 5-properties not valid format
        /// 6-prioritets is null
        /// </code>
        /// </summary>
        /// <param name="startPosition">Start position</param>
        /// <param name="pageSize">Page size of pogination</param>
        /// <param name="properties">Properties soderjatsya znachenie iskomih polei</param>
        /// <param name="prioritets">ukazivaetsya prioriteti</param>
        /// <returns></returns>
        public ActionResult Search(int? startPosition, int? pageSize, string properties, string prioritets)
        {
            var errorResult = new Error() { Code = 0, Description = "OK", Result="" };
            if (startPosition <= 0 || startPosition==null)
            {
                errorResult.Code = 1;
                errorResult.Description = "startPosition is not valid";
                return Json(errorResult, JsonRequestBehavior.AllowGet);
            }
            if (pageSize <= 0 || pageSize==null)
            {
                errorResult.Code = 2;
                errorResult.Description = "pageSize is not valid";
                return Json(errorResult, JsonRequestBehavior.AllowGet);
            }
            if (String.IsNullOrEmpty(properties))
            {
                errorResult.Code = 3;
                errorResult.Description = "properties is null";
                return Json(errorResult, JsonRequestBehavior.AllowGet);
            }
            if (String.IsNullOrEmpty(prioritets))
            {
                errorResult.Code = 4;
                errorResult.Description = "prioritets is null";
                return Json(errorResult, JsonRequestBehavior.AllowGet);
            }
            if (properties.Count(x=>x==',')<4)
            {
                errorResult.Code = 5;
                errorResult.Description = "properties not valid format";
                return Json(errorResult, JsonRequestBehavior.AllowGet);
            }
            if (prioritets.Count(x=>x==',')<4)
            {
                errorResult.Code = 6;
                errorResult.Description = "prioritets is null";
                return Json(errorResult, JsonRequestBehavior.AllowGet);
            }
            var searchPoints = GetPointsPogination(startPosition, pageSize, properties,prioritets);
            var jsSerialz = new JavaScriptSerializer { MaxJsonLength = 100000000 };
            var jso = jsSerialz.Serialize(searchPoints);
            errorResult.Result = jso;
            
            return Json(errorResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> SetCheckedConfirm(int? id, bool? confirmChecked)
        {
            var errorResult = new Error() { Code = 0, Description = "OK", Result = "" };
            if (id == null)
            {
                errorResult.Code = 1;
                errorResult.Description = "id is null!";
                return Json(errorResult, JsonRequestBehavior.AllowGet);
            }
            if (confirmChecked == null)
            {
                errorResult.Code = 2;
                errorResult.Description = "confirmChecked is null!";
                return Json(errorResult, JsonRequestBehavior.AllowGet);
            }

            var pointAction = await _pointsForActionRepository.GetBy(x => x.Id == id).FirstOrDefaultAsync();
            if (pointAction == null)
            {
                errorResult.Code = 3;
                errorResult.Description = "pointAction not founded!";
                return Json(errorResult, JsonRequestBehavior.AllowGet);
            }
            pointAction.IsConfirm = confirmChecked;
            _pointsForActionRepository.Edit(pointAction);
            _pointsForActionRepository.Save();
            return Json(errorResult, JsonRequestBehavior.AllowGet);
        }
    }
}