﻿function normalFormatDate(date) {
    ///<summary>Return normal format date</summary>
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
    var yyyy = date.getFullYear();
    var hh = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    return date = dd + '/' + mm + '/' + yyyy + ' ' + hh + ':' + min + ':' + sec;
}

function viewPoints(point) {
    //var dateSend = Date.parse(point.DateSendSms);
    //var dateSend2 = normalFormatDate(point.DateSendSms);
    $("#pointsTbody").append('<tr class="points"> ' +
        '<td id="numberItem">1</td>' +
        '<td id="NumberPoint_Text">'+point.NumberPoint+'</td>' +
        '<td id="FirstName_Text">'+point.FirstName+'</td>' +
        '<td id="LastName_Text">'+point.LastName+'</td>' +
        '<td id="PhoneNumber_Text">'+point.PhoneNumber+'</td>' +
        '<td id="PhoneNumber_Text">' + point.DateSendSmsString + '</td>' +
        '</tr>');
}
function checkNumberPoint() {
    ///<summary>Proverka nomera kupona na sushestvovanie</summary>
    $("#pointsTbody").empty();
    var textSearch = $("#searchNumber");

    $.ajax({
        url: '/Home/CheckPointNumber',
        type: 'GET',
        data: {
            pointNumber: textSearch[0].value
        },
        success: function (response) {
            if (response.Code != 0) {
                alert(response.Description);
            } else {
                viewPoints(JSON.parse(response.Result));
            }
        }
    });
}