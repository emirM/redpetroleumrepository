﻿function search(points, searchText, indexSearch) {
    ///<summary>poisk
    ///points-elementi po kotorim budet osushestevlena poisk
    ///searchText-iskomiy text
    ///indexSearch - index poiska, to est posledovatelnost poiska. kakoi perviy, vtoroi
    ///</summary>
    searchText.parentElement.children[1].value = indexSearch;//indexSearch
    searchText.parentElement.children[2].value = 1;//isSearch
    for (var i = 0; i < points.length; i++) {
        var res = points[i].children[searchText.parentElement.cellIndex].innerText;
        var searchtext = searchText.value;
        var result = res.search(new RegExp(searchtext, "i"));
        if (result >=0) {
            points[i].style.display = "";
        } else {
            points[i].style.display = "none";
        }
    }
}

function changedInputValue(idInput) {
    var inputVal = $("#" + idInput);

    if (inputVal.length > 0) {
        var allPoints = $(".points");//vitaskivaem vse poiti
        var points = JSLINQ(allPoints).Where(function (item) { return item.style.display != "none" });

        var indexSearchs = $(".isSearch");
        var isSearch = JSLINQ(indexSearchs).Where(function (item) { return item.value == 1 });//ishem bilo do etogo osushestevlen poisk
        var orderBy_indexSearchsIsSearch = JSLINQ(isSearch.items).OrderBy(function (item) { return item.parentElement.children[1].value });//sortirovka index poiska\

        if (points.items.length > 0) {//esli oni est
           
            if (isSearch.items.length > 0) { //esli da to
                //inputVal[0].parentElement.children[2].value - znachenie isSearch
                if (inputVal[0].value == "" || inputVal[0].parentElement.children[2].value==1) {//esli iskomoe znachenie pustoe ili po nim uje bil poisk togda poisk bydet zanogog
                    //if (inputVal[0].parentElement.children[2].value == 0) {
                        //poisk zanogo
                    for (var i = 0; i < orderBy_indexSearchsIsSearch.items.length; i++) {
                        if (i > 0) {
                            var pointsFilter = JSLINQ(allPoints).Where(function (item) { return item.style.display != "none" });
                            search(pointsFilter.items, orderBy_indexSearchsIsSearch.items[i].parentElement.children[0], i);
                        } else {
                            search(allPoints, orderBy_indexSearchsIsSearch.items[i].parentElement.children[0], i);
                        }
                   }
                    //}
                } else {
                    search(points.items, inputVal[0], orderBy_indexSearchsIsSearch.items.length);
                }
            } else {//esli net
                if (inputVal[0].value != "") {
                    search(points.items, inputVal[0], 0);
                }
            }
        } else if (allPoints.length > 0) {
            for (var j = 0; j < orderBy_indexSearchsIsSearch.items.length; j++) {
                if (j > 0) {
                    var pointsFilter2 = JSLINQ(allPoints).Where(function (item) { return item.style.display != "none" });
                    search(pointsFilter2.items, orderBy_indexSearchsIsSearch.items[j].parentElement.children[0], j);
                } else {
                    search(allPoints, orderBy_indexSearchsIsSearch.items[j].parentElement.children[0], j);
                }
            }
        }
    }
}

function viewResult(results) {
    $("#elementsViewBody").empty();
    var lastPageNumber = $("#lastPageNumber");
    var activePage = $("#pagination .active");//now page
    var activePageNumber = parseInt(activePage[0].innerText)-1;
    var nextLastPageNumber = 20 * activePageNumber;
    for (var i = 0; i < results.length; i++) {
        nextLastPageNumber = parseInt(nextLastPageNumber) + 1;
        var checked = results[i].IsConfirm == true ? "checked" : "";
        $("#elementsViewBody").append('<tr class="points" id="'+results[i].Id+'">' +
            '<td id="numberItem">' + nextLastPageNumber + '</td>' +
            '<td id="NumberPoint_Text">'+results[i].NumberPoint+'</td>' +
            '<td id="FirstName_Text">' + results[i].FirstName + '</td>' +
            '<td id="LastName_Text">' + results[i].LastName + '</td>' +
            '<td id="PhoneNumber_Text">' + results[i].PhoneNumber + '</td>' +
            '<td id="DateSendSms_Text">' + results[i].DateSendSmsString + '</td>' +
            '<td id="IsConfirm_Text"><input onclick="checkedConfirmForm(' + results[i].Id + ')"  class="form-control input-sm" type="checkbox" ' + checked + '/></td>' +
            '</tr>');
    }
    lastPageNumber[0].value = nextLastPageNumber;
}

function searchNew(startPosition, pageSize, properties, prioritets) {
    if (startPosition > 1) {
        startPosition = (parseInt(startPosition) - 1) * pageSize;
    }
    $.ajax({
        url: '/Home/Search',
        type: 'GET',
        data: {
            startPosition: startPosition,
            pageSize: pageSize,
            properties: properties,
            prioritets: prioritets
        },
        success: function(result) {
            if (result.Code == 0) {
                var res = JSON.parse(result.Result);
                viewResult(res);
            }
        },
        error: function(result) {
            var str = result.responseText.split('{');
            var resultStr = JSON.parse(result.responseText.substr(str[0].length));
            if (resultStr.Code == 0) {
                viewResult(JSON.parse(resultStr.Result));
            }
        }
    });
    //.done(function (result) {
    //      alert("success");
    //  })
    //.fail(function (result) {
    //    //alert("error");
    //         // var jso = JSON.parse(result.responseText);
    //          //viewResult(jso);
    //})
    //.always(function (result) {
    //    //viewResult(result.responseText);
    //          alert(result.responseText);
    //      });
}

function changedInputValueNew(idInput) {
    var result = {
        "code": 0,
        "description": "ok"
    };

    var inputVal = $("#" + idInput);
    if (inputVal.length <= 0) {
        result.code = 1;
        result.description = "element s takim imenem ne naideno!";
        return result;
    }

    var inputNumberPoint = $("#searchInputs #NumberPoint");
    if (inputNumberPoint.length <= 0) {
        result.code = 2;
        result.description = "element s takim imenem ne naideno!";
        return result;
    }

    var inputFirstName = $("#searchInputs #FirstName");
    if (inputFirstName.length <= 0) {
        result.code = 3;
        result.description = "element s takim imenem ne naideno!";
        return result;
    }

    var inputLastName = $("#searchInputs #LastName");
    if (inputLastName.length <= 0) {
        result.code = 4;
        result.description = "element s takim imenem ne naideno!";
        return result;
    }

    var inputPhone = $("#searchInputs #Phone");
    if (inputPhone.length <= 0) {
        result.code = 5;
        result.description = "element s takim imenem ne naideno!";
        return result;
    }
    var inputFirstNameRes = inputFirstName[0].value == "" ? "" : "'"+inputFirstName[0].value+"'";
    var inputLastNameRes = inputLastName[0].value == "" ? "" : "'" + inputLastName[0].value + "'";
    var inputPhoneRes = inputPhone[0].value == "" ? "" : "'" + inputPhone[0].value + "'";

    var properties = inputNumberPoint[0].value + "," + inputFirstNameRes + "," + inputLastNameRes + "," + inputPhoneRes + ",,";
    var prioritets = "0,1,2,3,4";//inputNumberPoint[0].nextElementSibling.value + "," + inputFirstName[0].nextElementSibling.value + "," + inputLastName[0].nextElementSibling.value + "," + inputPhone[0].nextElementSibling.value + ",4";
    searchNew(1, 200, properties, prioritets);

    return result;
}

function getPropertiesAndPrioritet() {
    var numberPoint = $("#NumberPoint")[0].value;
    var indexSearch_NumberPoint = $("#indexSearch_NumberPoint")[0].value;

    var firstName = $("#FirstName")[0].value;
    var indexSearch_FirstName = $("#indexSearch_FirstName")[0].value;

    var lastName = $("#LastName")[0].value;
    var indexSearch_LastNamex = $("#indexSearch_LastNamex")[0].value;

    var phone = $("#Phone")[0].value;
    var indexSearch_Phone = $("#indexSearch_Phone")[0].value;

    var result = {
        "properties": numberPoint+ "," + firstName+ "," + lastName + "," + phone+",,",
        "prioritets": indexSearch_NumberPoint + "," + indexSearch_FirstName + "," + indexSearch_LastNamex + "," + indexSearch_Phone + ",4"
    };
    return result;
}

function clearInputSearch() {

    $("#NumberPoint").val("");
    $("#FirstName").val("");
    $("#LastName").val("");
    $("#LastName").val("");
    $("#Phone").val("");
}

function lastPage(page) {
    clearInputSearch();
    var activePage = $("#pagination .active");//now page
    activePage.removeClass("active");
    var totalCountPage = $("#totalCountPage");//all pages
    totalCountPage[0].className += " active";

    //var propertiesAndPrioritet = getPropertiesAndPrioritet();
    searchNew(page, 20, ",,,,,", "0,1,2,3,4");
    
}

function pageCurrent(pagePosittion) {
    clearInputSearch();
    var activePage = $("#pagination .active");//now page
    activePage.removeClass("active");
    var pages = $(".page");//all pages
    pages[pagePosittion].className += " active";
    searchNew(pages[pagePosittion].children[0].innerText, 20, ",,,,,", "0,1,2,3,4");
}

function prevPage() {
    clearInputSearch();
    var result = {
        "code": 0,
        "description": "ok"
    };
    var pagination = $("#pagination");
    var totalCountPage = $("#fullCountPageInput");
    var totalCountAllPage = parseInt(totalCountPage[0].value) - 1;
    $("#nextPage").removeClass("disabled");
    var activePage = $("#pagination .active");//now page
    if (activePage[0].innerText == 1) {
        result.code = 1;
        result.description = "this is last page!";
        return result;
    }
    var pages = $(".page");//all pages
    for (var i = 0; i < pages.length; i++) {//all pages in cicle
        if (activePage[0].innerText == pages[i].innerText) { //if this now page
            activePage.removeClass("active");
            var activePageSplit = activePage[0].id.split(' ');
            if (activePageSplit[0]== "firstPage") { //esli eto posledniy po ryadu stranica

                $("#lastPage")[0].id = "";
                $("#firstPage")[0].id = "";
                var nowPageNumber = parseInt(pages[i].innerText);
                pages[0].children[0].innerText = nowPageNumber - 5; //peremeshaem posledniy element na 1-y
                //pages[j].className += " active";
                pages[1].children[0].innerText = nowPageNumber - 4;
                pages[2].children[0].innerText = nowPageNumber - 3;
                pages[3].children[0].innerText = nowPageNumber - 2;
                pages[4].children[0].innerText = nowPageNumber - 1;
                pages[0].id = "firstPage";
                pages[4].id = "lastPage";
                pages[4].className += " active";
                pages[0].style.display = "";
                pages[1].style.display = "";
                pages[2].style.display = "";
                pages[3].style.display = "";
                pages[4].style.display = "";
                ////var lastPageIndex = -1;
                //for (var j = 0; j < pages.length; j++) {
                //    if (parseInt(pages[i].innerText) >= parseInt(totalCountPage[0].value)) {
                //        result.code = 2;
                //        result.description = "this is last page!";
                //        return result;
                //    }
                //    if (j == 0) {
                //        pages[j].children[0].innerText = parseInt(pages[i].innerText) - 5; //peremeshaem posledniy element na 1-y
                //        //pages[j].className += " active";
                //        pages[j+1].children[0].innerText = parseInt(pages[i].innerText) - 4;
                //        pages[j + 2].children[0].innerText = parseInt(pages[i].innerText) - 3;
                //        pages[j + 3].children[0].innerText = parseInt(pages[i].innerText) - 2;
                //        pages[j].id = "lastPage";
                //        pages[j + 3].className += " active";
                //        $("#lastPage")[0].id = "";

                //    }
                //    if (pages[j].innerText == totalCountAllPage) {
                //        pages[j].id += " firstPage";
                //        //lastPageIndex = j;
                //        $("#nextPage")[0].className = "disabled";
                //    } else if (j > 0) {
                //        pages[j].style.display = "";
                //        pages[j].children[0].innerText = parseInt(pages[j - 1].innerText) + 1; //index perem pervui element
                //    }

                //}

                //pages[i].removeClass()
                //pages[i].children[0].innerText = parseInt(pages[i].innerText) + 1;
                //pages[i].innerText = pages[i].innerText + 1; //togda delaem minus 1
            } else {
                pages[i - 1].className += " active";
            }

            //if (pages[i].innerText == totalCountPage[0].value) {
            //    $("#nextPage")[0].className = "disabled";
            //}
        }
    }
    //var lastPage = $("#lastPage");
    //if (lastPage[0].innerText == totalCountAllPage) {
    //    $("#nextGroupPages")[0].style.display = "none";
    //}
    var startPosition = $("#pagination .active");
    searchNew(startPosition[0].innerText, 20, ",,,,,", "0,1,2,3,4");
    return result;
}

function nextPage() {
    clearInputSearch();
    var result = {
        "code": 0,
        "description": "ok"
    };
    var pagination = $("#pagination");
    var totalCountPage = $("#fullCountPageInput");
    var totalCountAllPage = parseInt(totalCountPage[0].value) - 1;
    var prevPage = $("#prevPage");//nahodim knopku prevPage
    if (prevPage[0].className == "disabled") {//esli eto knopka ne activna
        prevPage.removeClass("disabled");
        //prevPage[0].className = "active";//delaem activnim potomu chto 
    }
    var activePage = $("#pagination .active");//now page
    if (totalCountAllPage == activePage[0].innerText) {
        result.code = 1;
        result.description = "this is last page!";
        return result;
    }
    var pages = $(".page");//all pages
    var isLastPage = false;
    for (var i = 0; i < pages.length; i++) {//all pages in cicle
        if (activePage[0].innerText == pages[i].innerText) { //if this now page
            activePage.removeClass("active");
            if (activePage[0].id == "lastPage") { //esli eto posledniy po ryadu stranica
                var indexLastPage = 0;
                $("#lastPage")[0].id = "";
                $("#firstPage")[0].id = "";
                var nowPageNumber = parseInt(activePage[0].innerText);
                pages[0].children[0].innerText = nowPageNumber + 1; //peremeshaem posledniy element na 1-y
                pages[0].id = "firstPage";
                pages[0].className += " active";
                pages[0].style.display = "";
                //pages[j].className += " active";
                if (nowPageNumber + 2 <= totalCountAllPage) {
                    pages[1].children[0].innerText = nowPageNumber + 2;
                    pages[1].style.display = "";
                } else {
                    pages[1].style.display = "none";
                }

                if (nowPageNumber + 3 <= totalCountAllPage) {
                    pages[2].children[0].innerText = nowPageNumber + 3;
                    pages[2].style.display = "";
                } else {
                    pages[2].style.display = "none";
                    indexLastPage = 1;
                }

                if (nowPageNumber + 4 <= totalCountAllPage) {
                    pages[3].children[0].innerText = nowPageNumber + 4;
                    pages[3].style.display = "";
                } else {
                    pages[3].style.display = "none";
                    indexLastPage = 2;
                }

                if (nowPageNumber + 5 <= totalCountAllPage) {
                    pages[4].children[0].innerText = nowPageNumber + 5;
                    pages[4].style.display = "";
                    indexLastPage = 4;
                } else {
                    pages[4].style.display = "none";
                    indexLastPage = 3;
                }
                pages[indexLastPage].id = "lastPage";

                    //isLastPage = true;//stavim flag chto eto perviy element
                    //var lastPageIndex = -1;
                    //for (var j = 0; j < pages.length; j++) {
                    //    if (parseInt(pages[i].innerText) >= parseInt(totalCountPage[0].value)) {
                    //        result.code = 2;
                    //        result.description = "this is last page!";
                    //        return result;
                    //    }
                    //    if (j == 0) {
                    //        pages[j].children[0].innerText = parseInt(pages[i].innerText) + 1; //peremeshaem posledniy element na 1-y
                    //        pages[j].className += " active";
                    //    }
                    //    if (j > 0 && pages[j - 1].innerText == totalCountAllPage) {
                    //        pages[j-1].id += " lastPage";
                    //        lastPageIndex = j - 1;
                    //        pages[j].style.display = "none";
                    //        pages[j].id = "";
                    //    } else if (j > 0) {
                    //        if (lastPageIndex >= 0 && lastPageIndex < j) {
                    //            pages[j].style.display = "none";
                    //            pages[j].id = "";
                    //        } else {
                    //            pages[j].children[0].innerText = (j == 0) ? parseInt(pages[j].innerText) + 1 : parseInt(pages[j - 1].innerText) + 1; //index perem pervui element
                    //        }
                    //    }
                        
                    //}
                    
                    //pages[i].removeClass()
                    //pages[i].children[0].innerText = parseInt(pages[i].innerText) + 1;
                    //pages[i].innerText = pages[i].innerText + 1; //togda delaem minus 1
                } else {
                pages[i + 1].className += " active";
            }

            //if (pages[i].innerText == totalCountPage[0].value) {
            //    $("#nextPage")[0].className = "disabled";
            //}
        }
    }
    //var lastPage = $("#lastPage");
    //if (lastPage[0].innerText == totalCountAllPage) {
    //    $("#nextGroupPages")[0].style.display = "none";
    //}
    var startPosition = $("#pagination .active");
    searchNew(startPosition[0].innerText, 20, ",,,,,", "0,1,2,3,4");
    return result;
}

//function nextGroupPages() {
//    var activePage = $("#pagination .active");//now page
//    activePage.removeClass("active");
//}