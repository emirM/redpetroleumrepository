﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RedPetroleumWeb.Startup))]
namespace RedPetroleumWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
